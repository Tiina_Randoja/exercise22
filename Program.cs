﻿using System.Collections.Generic;
using System;

namespace exercise22
{
    class program
    {
        static void Main(string[] args)
        {//Start the program with Clear();
        Console.Clear();
{      //TASK A List of 5 Movies
      
        
       var movies = new string [5] { "Inception", "Lives of Others", "Transcendence", "Kingdom of Heaven", "Hunt for the Wilderpeople"};

       Console.WriteLine(movies[0]);
       Console.WriteLine(movies[1]);
       Console.WriteLine(movies[2]);
       Console.WriteLine(movies[3]);
       Console.WriteLine(movies[4]);
       
       Console.WriteLine();
       Console.WriteLine("press any key to continue");
       Console.WriteLine();
       

       //TASK B Changing no. 0 and 3
       movies[0] = "Now You See Me";
       movies[3] = "The Dark Knight";


       Console.WriteLine();
       Console.WriteLine("press any key to continue");
       Console.WriteLine();
       
       //TASK C Movies in Alphapetical Order

       Array.Sort(movies);
       Console.WriteLine(string.Join(",",movies));

       Console.WriteLine();
       Console.WriteLine("press any key to continue");
       Console.WriteLine();

       //TASK D The Lenght of the Array
       var count = movies.Length;

       Console.WriteLine($"The lenght of the array is {count}");
       Console.WriteLine();
       
       //TASK E Print the whole array as a string
       var output = string.Join(",", movies);
       Console.WriteLine("Those are all..");
       Console.WriteLine(output);
       Console.WriteLine("The movies in the array");

        }
        //TASK F Places to eat
        var restaurants = new List<string> {"Macau", "Phoenix", "Talk of India, Barrio Brothers, Burger Fuel "};
  
        Console.WriteLine("\nSort");
        Console.WriteLine("restaurants.Sort()");

        //End the program with blank line and instructions
        Console.ResetColor();
        Console.WriteLine();
        Console.WriteLine("Press <Enter> to quit the program");
        Console.ReadKey();
            Console.WriteLine("");
        }
    }
}
